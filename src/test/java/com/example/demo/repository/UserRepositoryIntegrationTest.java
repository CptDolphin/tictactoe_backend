//package com.example.demo.repository;
//
//import static org.assertj.core.api.Assertions.assertThat;
//
//import com.example.tictactoe.repositories.UserRepository;
//import com.example.tictactoe.model.AppUser;
//import com.example.tictactoe.model.Role;
//import org.junit.Before;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.mockito.InjectMocks;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
//import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
//import org.springframework.test.web.servlet.MockMvc;
//import org.springframework.test.web.servlet.MockMvcBuilder;
//import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
//import org.springframework.test.web.servlet.setup.MockMvcBuilders;
//
//import java.util.HashSet;
//import java.util.Set;
//
//@RunWith(SpringJUnit4ClassRunner.class)
//public class UserRepositoryIntegrationTest {
//
//    @Autowired
//    private TestEntityManager manager;
//
//    private MockMvc mockMvc;
//
//    @Autowired
//    @InjectMocks
//    private UserRepository repository;
//
//    @Before
//    public void setUp() throws Exception {
//        mockMvc = MockMvcBuilders.standaloneSetup(repository).build();
//    }
//
//    @Test
//    public void shouldGetUserByLogin() throws Exception {
////        mockMvc.perform(
////                MockMvcRequestBuilders.get();
////        );
//        Role role = new Role("role1");
//        Set<Role> roles = new HashSet<>();
//        roles.add(role);
//
//        this.manager.persist(new AppUser("login1","name1","pass1",roles));
//        AppUser appUser = repository.getOne(1L);
//        assert (appUser.getLogin().equals("login1"));
//        assert(appUser.getName().equals("name1"));
//        assert(appUser.getPassword().equals("pass1"));
//        assert(appUser.getRoleSet().equals("role1"));
//
//}}
//
