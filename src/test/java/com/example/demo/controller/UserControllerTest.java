//package com.example.demo.controller;
//
//import static org.assertj.core.api.Assertions.assertThat;
//
//import com.example.tictactoe.model.AppUser;
//import com.example.tictactoe.model.Role;
//import com.example.tictactoe.repositories.UserRepository;
//import com.example.tictactoe.services.UserService;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
//import org.springframework.boot.test.mock.mockito.MockBean;
//
//import static org.mockito.Mockito.timeout;
//import static org.mockito.Mockito.verify;
//import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
//import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
//import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
//import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
//
//
//import static org.mockito.Mockito.when;
//import org.springframework.test.context.junit4.SpringRunner;
//import org.springframework.test.web.servlet.MockMvc;
//
//import java.util.HashSet;
//
//@RunWith(SpringRunner.class)
//@WebMvcTest
//public class UserControllerTest {
//
//    @Autowired
//    private MockMvc mockMvc;
//
//    @MockBean
//    private UserService userService;
//
//    @MockBean
//    private UserRepository userRepository;
//
//    @Test
//    public void shouldGetAllUsers() throws Exception{
//
//        Long id = 1L;
//
//        String login = "login1";
//        String name = "name1";
//        String pass = "pass1";
//
//        HashSet<Role> roles = new HashSet<>();
//        roles.add(new Role("admin"));
//
//        when(userService.getAppUserWithId(id))
//                .thenReturn(java.util.Optional.of(
//                        new AppUser(login,name,pass,roles)));
//
//        mockMvc.perform(get("/users/get" + id))
//                .andDo(print())
//                .andExpect(status().isNotFound());
//
//        verify(userService,timeout(1)).getAppUserWithId(id);
//    }
//}
