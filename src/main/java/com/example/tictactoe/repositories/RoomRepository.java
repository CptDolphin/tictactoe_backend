package com.example.tictactoe.repositories;

import com.example.tictactoe.model.Room;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface RoomRepository extends JpaRepository<Room, Long>{
    Optional<Room> findByName(String name);
}
