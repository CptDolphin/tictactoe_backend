package com.example.tictactoe.repositories;

import com.example.tictactoe.model.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface RoleRepository extends JpaRepository<Role, Long>{
    Role save(Role role);
    Optional<Role> findById(Long id);
    Optional<Role> findByName(String name);
}
