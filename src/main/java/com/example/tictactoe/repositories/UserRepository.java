package com.example.tictactoe.repositories;

import com.example.tictactoe.model.AppUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<AppUser,Long>{
    Optional<AppUser> findByLogin(String login);
    List<AppUser> findAll();
    AppUser save(AppUser appUser);
}
