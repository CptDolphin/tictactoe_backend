package com.example.tictactoe.components;

import com.example.tictactoe.model.Role;
import com.example.tictactoe.model.dto.AppUserRegisterDto;
import com.example.tictactoe.repositories.RoleRepository;
import com.example.tictactoe.services.IUserService;
import com.example.tictactoe.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;


@Component
public class DataInitializer {

    @Autowired
    private UserService userService;
    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    public DataInitializer(UserService userService, RoleRepository roleRepository) {
        this.userService = userService;
        this.roleRepository = roleRepository;
        initialize();
    }

//    @PostConstruct
    public void initialize() {
        if (!roleRepository.findByName("ADMIN").isPresent()) {
            roleRepository.save(new Role("ADMIN"));
        }

        if (!userService.getAppUserWithLogin("admin").isPresent()) {
            userService.registerUser(new AppUserRegisterDto("admin", "admin", "admin"));
        }
    }
}
