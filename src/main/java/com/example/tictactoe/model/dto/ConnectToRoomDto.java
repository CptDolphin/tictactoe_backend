package com.example.tictactoe.model.dto;

import lombok.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class ConnectToRoomDto {
    private Long roomId;
    private Long userId;
}
