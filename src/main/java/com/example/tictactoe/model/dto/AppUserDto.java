package com.example.tictactoe.model.dto;

import com.example.tictactoe.model.AppUser;
import com.example.tictactoe.model.Room;
import lombok.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class AppUserDto {
    private Long id;
    private String login;
    private String name;
    private Room room;

    public static AppUserDto create(AppUser user) {
        return new AppUserDto(
                user.getId(),
                user.getLogin(),
                user.getName(),
                user.getRoom());
    }
}
