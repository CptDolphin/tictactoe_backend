package com.example.tictactoe.model.dto;

import lombok.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class RoomCreateDto {
    private String name;
}
