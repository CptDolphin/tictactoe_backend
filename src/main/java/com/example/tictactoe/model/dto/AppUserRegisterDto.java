package com.example.tictactoe.model.dto;

import lombok.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class AppUserRegisterDto {
    private String login;
    private String name;
    private String password;

}
