package com.example.tictactoe.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.*;
import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Entity
public class AppUser implements UserDetails {
    @Id
    @Column(unique = true, nullable = false)
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String login;
    private String name;
    private String password;

    @OneToMany(fetch = FetchType.EAGER
            , cascade = CascadeType.PERSIST
    )
    private Set<Role> roleSet;

    private int gameWon;
    private int gameLost;

    public AppUser(String login, String name, String password, Set<Role> roleSet) {
        this.login = login;
        this.name = name;
        this.password = password;
        this.roleSet = roleSet;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JsonIgnore
    private Room room;

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return this.roleSet.stream()
                .map(role -> new SimpleGrantedAuthority(role.getName()))
                .collect(Collectors.toList());
    }

    @Override
    public String getUsername() {
        return login;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }
}
