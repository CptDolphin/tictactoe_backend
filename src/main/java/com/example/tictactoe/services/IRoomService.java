package com.example.tictactoe.services;

import com.example.tictactoe.model.Room;
import com.example.tictactoe.model.dto.ConnectToRoomDto;
import com.example.tictactoe.model.dto.RoomCreateDto;

import java.util.List;
import java.util.Optional;

public interface IRoomService {
    Optional<Room> createRoom(RoomCreateDto roomCreateDto);
    Optional<Room> getRoomById(Long id);
    List<Room> getAllRooms();
    Optional<Room> connectToRoom(ConnectToRoomDto dto);
}
