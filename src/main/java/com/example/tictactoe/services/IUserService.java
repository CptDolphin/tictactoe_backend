package com.example.tictactoe.services;

import com.example.tictactoe.model.AppUser;
import com.example.tictactoe.model.Room;
import com.example.tictactoe.model.dto.AppUserDto;
import com.example.tictactoe.model.dto.AppUserRegisterDto;
import com.example.tictactoe.model.dto.ConnectToRoomDto;
import org.springframework.http.ResponseEntity;

import java.util.List;
import java.util.Optional;

public interface IUserService {
    Optional<AppUserDto> registerUser(AppUserRegisterDto user);
    Optional<AppUser> getAppUserWithId(Long id);
    AppUser addToRoom(AppUser user, Room room);
    List<AppUser> getAllUsers();
}
