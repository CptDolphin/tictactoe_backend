package com.example.tictactoe.services;

import com.example.tictactoe.exceptions.UserIsInvalidException;
import com.example.tictactoe.exceptions.UserWithThatLoginExistsException;
import com.example.tictactoe.model.AppUser;
import com.example.tictactoe.model.Role;
import com.example.tictactoe.model.Room;
import com.example.tictactoe.model.dto.AppUserDto;
import com.example.tictactoe.model.dto.AppUserRegisterDto;
import com.example.tictactoe.model.dto.LoginDto;
import com.example.tictactoe.repositories.RoleRepository;
import com.example.tictactoe.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.logging.Logger;

//@Component
@Service
public class UserService implements IUserService {
    private static final Long CURRENT_PRIVELAGE_OF_ADMIN = 1L;
//    private static final Logger logger = Logger.getLogger(String.valueOf(UserService.class));

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Override
    public Optional<AppUserDto> registerUser(AppUserRegisterDto user) {
        System.out.println("Process to register user started, user:"  + user.toString());
        if (isInvalidUser(user)) {
            throw new UserIsInvalidException();
        }
        if (isLoginRegistered(user.getLogin())) {
            throw new UserWithThatLoginExistsException();
        }

        HashSet<Role> set = new HashSet<>();
        set.add(roleRepository.findByName("ADMIN").get());

        AppUser userToRegister = new AppUser(user.getLogin(), user.getName(), user.getPassword(), set);

        userToRegister.setPassword(bCryptPasswordEncoder.encode(userToRegister.getPassword()));

        userRepository.save(userToRegister);
        return Optional.ofNullable(AppUserDto.create(userToRegister));
    }

    @Override
    public Optional<AppUser> getAppUserWithId(Long id) {
        return userRepository.findById(id);
    }

    public Optional<AppUser> getAppUserWithLogin(String login) {
        return userRepository.findByLogin(login);
    }

    private boolean isLoginRegistered(String login) {
        return userRepository.findByLogin(login).isPresent();
    }

    private boolean isInvalidUser(AppUserRegisterDto user) {
        return user.getLogin() == null ||
                user.getLogin().isEmpty() ||
                user.getPassword() == null ||
                user.getPassword().isEmpty();
    }

    public AppUser addToRoom(AppUser user, Room room) {
        user.setRoom(room);
        user = userRepository.save(user);
        return user;

    }

    public Optional<AppUser> getUserWithLoginAndPassword(LoginDto dto) {
        Optional<AppUser> userFromDB = userRepository.findByLogin(dto.getLogin());
        if (userFromDB.isPresent()) {
            AppUser user = userFromDB.get();
            if (bCryptPasswordEncoder.matches(dto.getPassword(), user.getPassword())) {
            }
            return userFromDB;
        }
        return Optional.empty();
    }

    public List<AppUser> getAllUsers() {
        return userRepository.findAll();
    }
}
