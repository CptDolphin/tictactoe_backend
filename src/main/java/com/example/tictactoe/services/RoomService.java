package com.example.tictactoe.services;

import com.example.tictactoe.exceptions.RoomExistsException;
import com.example.tictactoe.exceptions.WrongRoomNameException;
import com.example.tictactoe.model.AppUser;
import com.example.tictactoe.model.Room;
import com.example.tictactoe.model.dto.ConnectToRoomDto;
import com.example.tictactoe.model.dto.RoomCreateDto;
import com.example.tictactoe.repositories.RoomRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class RoomService implements IRoomService{

    @Autowired
    private RoomRepository roomRepository;

    @Autowired UserService userService;

    public Optional<Room> createRoom(RoomCreateDto roomCreateDto) {
        if(isWrongName(roomCreateDto)){
            throw new WrongRoomNameException();
        }
        if(roomExists(roomCreateDto.getName())){
            throw new RoomExistsException();
        }
        Room newRoom = new Room();
        newRoom.setName(roomCreateDto.getName());

        newRoom = roomRepository.save(newRoom);
        return Optional.ofNullable(newRoom);
    }

    private boolean isWrongName(RoomCreateDto roomCreateDto) {
        return roomCreateDto == null || roomCreateDto.getName() == null || roomCreateDto.getName().isEmpty();
    }

    private boolean roomExists(String name) {
        return roomRepository.findByName(name).isPresent();
    }

    public Optional<Room> getRoomById(Long id) {
        return roomRepository.findById(id);
    }

    public List<Room> getAllRooms() {
        return roomRepository.findAll();
    }

    public Optional<Room> connectToRoom(ConnectToRoomDto connectDto) {
        Optional<Room> roomOptional = roomRepository.findById(connectDto.getRoomId());
        if(roomOptional.isPresent()){
            Room room = roomOptional.get();

            Optional<AppUser> optionalUser = userService.getAppUserWithId(connectDto.getUserId());
            if(optionalUser.isPresent()){
                AppUser user = optionalUser.get();
                room.getConnectedUsers().add(user);

                roomRepository.save(room);
                userService.addToRoom(user,room);

                return Optional.ofNullable(room);
            }
        }
        return Optional.empty();
    }
}
