package com.example.tictactoe.controllers;

import com.example.tictactoe.configuration.JWTFilter;
import com.example.tictactoe.model.AppUser;
import com.example.tictactoe.model.Role;
import com.example.tictactoe.model.dto.AppUserDto;
import com.example.tictactoe.model.dto.AppUserRegisterDto;
import com.example.tictactoe.model.dto.AuthenticationDto;
import com.example.tictactoe.model.dto.LoginDto;
import com.example.tictactoe.response.ResponseFactory;
import com.example.tictactoe.services.UserService;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import javax.xml.bind.DatatypeConverter;
import java.security.Key;
import java.util.Date;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import static com.example.tictactoe.configuration.JWTFilter.SECRET;

@RestController
@CrossOrigin
@RequestMapping(path = "/auth/")
public class AuthenticationController {

    @Autowired
    private UserService userService;


    @RequestMapping(path = "/register", method = RequestMethod.POST)
    public ResponseEntity register(@RequestBody AppUserRegisterDto dto) {
        System.out.println("Someone tries to register /auth/register");
        Optional<AppUserDto> user = userService.registerUser(dto);
        if (user.isPresent()) {
            System.out.println("There is user - successful registration");
            return ResponseFactory.created(user.get());
        }
        System.out.println("No user - bad request");
        return ResponseFactory.badRequest();
    }

    @RequestMapping(path = "/authenticate", method = RequestMethod.POST)
    public ResponseEntity authenticate(@RequestBody LoginDto loginDto) {
        System.out.println("First came");
        Optional<AppUser> appUserOptional = userService.getUserWithLoginAndPassword(loginDto);
        System.out.println("DB found user");
        if (appUserOptional.isPresent()) {
            AppUser user = appUserOptional.get();
            SignatureAlgorithm signatureAlgorithm = SignatureAlgorithm.HS256;
            byte[] apiKeySecretBytes = DatatypeConverter.parseBase64Binary(SECRET);
            Key signingKey = new SecretKeySpec(apiKeySecretBytes, signatureAlgorithm.getJcaName());
            String token = Jwts.builder()
                    .setSubject(user.getLogin())
                    .setIssuedAt(new Date())
                    .claim("roles", translateRoles(user.getRoleSet()))
                    .signWith(signatureAlgorithm, signingKey)
                    .compact();
            System.out.println("Token is: " + token);
//            return ResponseFactory.ok(user);
            return ResponseFactory.ok(new AuthenticationDto(user.getLogin(), user.getId(), token));
        }
        return ResponseFactory.badRequest();
    }

    private Set<String> translateRoles(Set<Role> roleSet) {
        return roleSet.stream().map(role -> role.getName()).collect(Collectors.toSet());
    }
}
