package com.example.tictactoe.controllers;

import com.example.tictactoe.exceptions.RoomExistsException;
import com.example.tictactoe.exceptions.WrongRoomNameException;
import com.example.tictactoe.model.Room;
import com.example.tictactoe.model.dto.ConnectToRoomDto;
import com.example.tictactoe.model.dto.RoomCreateDto;
import com.example.tictactoe.response.ResponseFactory;
import com.example.tictactoe.services.RoomService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@CrossOrigin
@RequestMapping(path = "/room/")
public class RoomController {

    @Autowired
    private RoomService roomService;

    @RequestMapping(path = "/add", method = RequestMethod.POST)
    public ResponseEntity<Room> create(@RequestBody RoomCreateDto roomCreateDto) {
        try {
            Optional<Room> room = roomService.createRoom(roomCreateDto);

            if (room.isPresent()) {
                return ResponseFactory.created(room.get());
            }
        } catch (RoomExistsException ex) {
            ex.printStackTrace();
        } catch (WrongRoomNameException ex) {
            ex.printStackTrace();
        }
        return ResponseFactory.badRequest();
    }

    @RequestMapping(path = "/get/{id}", method = RequestMethod.GET)
    public ResponseEntity<Room> get(@PathVariable(name = "id") Long id) {
        Optional<Room> room = roomService.getRoomById(id);
        if (room.isPresent()) {
            return ResponseFactory.ok(room.get());
        }
        return ResponseFactory.badRequest();
    }

    @RequestMapping(path = "/getAll", method = RequestMethod.GET)
    public ResponseEntity<List<Room>> getAll() {
        List<Room> rooms = roomService.getAllRooms();
        return ResponseFactory.ok(rooms);
    }

    @RequestMapping(path = "/connect", method = RequestMethod.POST)
    public ResponseEntity<Room> connect(@RequestBody ConnectToRoomDto dto) {
        Optional<Room> optionalRoom = roomService.connectToRoom(dto); {
            if (optionalRoom.isPresent()) {
                return ResponseFactory.ok(optionalRoom.get());
            }
        return ResponseFactory.badRequest();
        }
    }

    //TODO: DELETEING ROOM
    //TODO: controler/esrvice/repository for game


}
