package com.example.tictactoe.controllers;

import com.example.tictactoe.exceptions.UserErrorException;
import com.example.tictactoe.model.AppUser;
import com.example.tictactoe.model.dto.AppUserDto;
import com.example.tictactoe.model.dto.AppUserRegisterDto;
import com.example.tictactoe.response.ResponseFactory;
import com.example.tictactoe.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@CrossOrigin
@RequestMapping(path = "/users/")
public class UserController {

    @Autowired
    private UserService userService;

    @RequestMapping(path = "/add",method = RequestMethod.POST)
    public ResponseEntity<AppUserDto> addUser(
            @RequestBody AppUserRegisterDto user, BindingResult result){
        try{
            Optional<AppUserDto> registeredUser = userService.registerUser(user);
            if(registeredUser.isPresent()){
                return ResponseFactory.created(registeredUser.get());
            }
        }catch (UserErrorException ex){
            ex.printStackTrace();
        }
        return ResponseFactory.badRequest();
    }

    @RequestMapping(path = "/get", method = RequestMethod.GET)
    public ResponseEntity<AppUser> getAppUser(@RequestParam(name = "/id")Long id){
        Optional<AppUser> user = userService.getAppUserWithId(id);
        return user.map(ResponseFactory::ok).orElseGet(ResponseFactory::badRequest);
    }

    @RequestMapping(path = "/get/{id}", method = RequestMethod.GET)
    public ResponseEntity<AppUser> getAppUserWithId(@RequestAttribute(name = "id") Long id) {
        Optional<AppUser> user = userService.getAppUserWithId(id);
        return user.map(ResponseFactory::ok).orElseGet(ResponseFactory::badRequest);
    }

    @RequestMapping(path = "/getAll",method = RequestMethod.GET)
    public ResponseEntity<List<AppUser>> getAll (){
        List<AppUser> users = userService.getAllUsers();
        return ResponseFactory.ok(users);
    }
//    @RequestMapping(path = "/getAll",method = RequestMethod.GET)
//    public List<AppUser> getAll (){
//        List<AppUser> users = userService.getAllUsers();
//        return users;
//    }
}
